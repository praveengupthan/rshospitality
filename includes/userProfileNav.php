 <nav class="navbar navbar-expand-lg">
    <button class="navbar-togglerProfile" type="button" data-bs-toggle="collapse" data-bs-target="#ProfileNavPostLogin" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
      Navigation
    </button>
     <div class="collapse navbar-collapse" id="ProfileNavPostLogin">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item"><a class="active" href="customerdashboard.php" class="active">Dashboard</a></li>
            <li class="nav-item"><a class="nav-link" href="customerProfile.php">My Profile</a></li>
            <li class="nav-item"><a class="nav-link" href="customerVisitInformation.php">Visit Information</a></li>
            <li class="nav-item"><a class="nav-link" href="customerreports.php">Reports</a></li>
            <li class="nav-item"><a class="nav-link" href="javascript:void(0)">Current Promos</a></li>
            <li class="nav-item"><a class="nav-link" href="javascript:void(0)">Change Password</a></li>
            <li class="nav-item"><a class="nav-link" href="javascript:void(0)">Logout</a></li>
        </ul>
     </div>
 </nav>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RS Hospitality</title>
     <?php include 'includes/styles.php' ?>
     <?php include 'includes/arrayObjects.php' ?>
</head>

<body>    
   <?php include 'includes/header.php'?>
    <!-- sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="subpageHeader">
            <div class="container">
                <h1 class="h1">All Campaigns</h1>
            </div>
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpageBody">
            <div class="container-fluid">

            <!-- row -->
            <div class="row justify-content-center">
                <?php 
                for ($i=0; $i<count($campaignItem); $i++ ) {?>
                <div class="col-md-6 col-lg-4 col-xl-3">
                    <div class="campDiv mb-3">
                        <a href="javascript:void(0)">
                            <img src="img/campimg/<?php echo $campaignItem [$i][0]?>.jpg" alt="" class="img-fluid w-100">
                        </a>
                        <article>
                            <h4 class="h5">
                                <a href="javascript:void(0)"><?php echo $campaignItem [$i][1]?></a>
                            </h4>
                            <p class="d-flex justify-content-between loc">
                                <span><?php echo $campaignItem [$i][2]?></span>
                                <span>Offer Ends: <?php echo $campaignItem [$i][3] ?></span>
                            </p>
                            <p> <?php echo $campaignItem [$i][4]?></p>
                            <p class="text-center pt-3">
                                <a href="javascript:void(0)" class="borderLink" data-bs-toggle="modal" data-bs-target="#viewCampaign">View Campaign</a>
                            </p>
                        </article>
                    </div>
                </div>   
                <?php } ?>            
            </div>
            <!--/ row -->

              
               
               
            </div>
        </div>        
        <!--/ sub page body -->
    </main>
    
    <!--/ sub page main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php' ?>

     <!-- Modal -->
    <div class="modal fade" id="viewCampaign" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-fullscreen">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Campaign Name will be here</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <figure class="camppopimg">
                                <img src="img/licensesandpermits.png" alt="" class="img-fluid">
                            </figure>
                        </div>

                        <div class="col-md-9">                        
                            <div class="row">
                                <div class="col-md-12">
                                    <small class="pb-3 d-block">Anniversary Offer lasts on september p,2021.</small>
                                </div>
                                <div class="col-md-4">
                                    <h5>Campaign Start Date</h5>
                                    <p>09 Sep,2021</p>
                                </div>
                                <div class="col-md-4">
                                    <h5>Campaign End Date</h5>
                                    <p>09 Sep,2021</p>
                                </div>
                                <div class="col-md-4">
                                    <h5>Draw Date</h5>
                                    <p>15-05-2021</p>
                                </div>
                                <div class="col-md-4">
                                    <h5>Communicate Date</h5>
                                    <p>09 Sep,2021</p>
                                </div>
                                <div class="col-md-4">
                                    <h5>Distribution Date</h5>
                                    <p>10 Sep,2021</p>
                                </div>
                                <div class="col-md-4">
                                    <h5>Amount</h5>
                                    <p>14000.00</p>
                                </div>
                                <div class="col-md-4">
                                    <h5>Gift</h5>
                                    <p>Smart Phone</p>
                                </div>
                            </div>
                        </div>                       
                     </div>
                          
                </div>
            </div>
                <div class="modal-footer">
                     <!-- <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button> -->
                     <button type="button" class="btn btn-primary filledLink">Save</button>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
<!-- scripts -->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bsnav.min.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <!--[if lte IE 9]>
            <script src="js/ie.lteIE9.js"></script>
        <![endif]-->
    <script src="https://cdn.datatables.net/1.11.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.1/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>

    <script src="js/custom.js"></script>
    <!-- scripts -->   
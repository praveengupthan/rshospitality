<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RS Hospitality</title>
     <?php include 'includes/styles.php' ?>
     <?php include 'includes/arrayObjects.php' ?>
</head>

<body>    
   <?php include 'includes/header.php'?>
    <!-- sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="subpageHeader">
            <div class="container">
                <h1 class="h1">Login</h1>
            </div>
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpageBody">
            <div class="container">

                <!-- row -->
                <div class="row justify-content-center">
                        <!-- col -->
                        <div class="col-lg-6">
                            <p>Welcome back! Sign in to your account</p>
                            <form class="form py-4 signform">
                                <div class="form-group">
                                    <label>Username or Email</label>
                                    <input type="text" class="form-control" placeholder="Username">
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" class="form-control" placeholder="Password">
                                </div>
                                <div class="form-group d-flex justify-content-between">
                                     <input onclick="window.location.href='customerProfile.php';" type="button" class="filledLink" value="Submit">
                                     <a href="forgotpw.php" class="fbold">Forgot Password?</a>
                                </div>                              
                                <div class="form-group d-flex justify-content-between">
                                    <span>New Customer? <a class="fbold" href="customerRegistration.php"> Register</a></span>
                                    <span>New Business? <a class="fbold" href="joinasbusiness.php"> Register</a></span>
                                </div>
                            </form>
                        </div>
                        <!--/ col -->
                    </div>
                <!--/ row -->
            </div>
        </div>        
        <!--/ sub page body -->
    </main>
    
    <!--/ sub page main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php' ?>
</body>

</html>
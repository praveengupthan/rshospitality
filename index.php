<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RS Hospitality</title>
     <?php include 'includes/styles.php' ?>
     <?php include 'includes/arrayObjects.php' ?>
</head>

<body>    
   <?php include 'includes/header.php'?>
    <!-- main -->
    <!-- campaigns -->
    <section class="homeCampaigns">
        <div class="customContainer">
            <div class="row">
                <div class="col-8">
                    <h3 class="sectionTitle py-3">Latest Campaigns</h3>
                </div>
                <div class="col-4 text-md-end align-self-center">
                    <a href="javascript:void(0)" class="borderLink">View All</a>
                </div>
            </div>
            <!-- row -->
            <div class="row justify-content-center">
                <?php 
                for ($i=0; $i<count($campaignItem); $i++ ) {?>
                <div class="col-md-6 col-lg-4 col-xl-3">
                    <div class="campDiv mb-3">
                        <a href="javascript:void(0)">
                            <img src="img/campimg/<?php echo $campaignItem [$i][0]?>.jpg" alt="" class="img-fluid w-100">
                        </a>
                        <article>
                            <h4 class="h5">
                                <a href="javascript:void(0)"><?php echo $campaignItem [$i][1]?></a>
                            </h4>
                            <p class="d-flex justify-content-between loc">
                                <span><?php echo $campaignItem [$i][2]?></span>
                                <span>Offer Ends: <?php echo $campaignItem [$i][3] ?></span>
                            </p>
                            <p> <?php echo $campaignItem [$i][4]?></p>
                            <p class="text-center pt-3">
                                <a href="javascript:void(0)" class="borderLink">View Campaign</a>
                            </p>
                        </article>
                    </div>
                </div>   
                <?php } ?>            
            </div>
            <!--/ row -->
        </div>
    </section>
    <!--/ campaigns-->
    <!-- how it works -->
    <section class="howitworks">
        <!-- container -->
        <div class="container">
            <h3 class="sectionTitle py-3 text-center">How it works</h3>
            <!-- row -->
            <div class="row">
                <!-- col -->
                <?php 
                for($i=0; $i<count($howItworkscol); $i++) {?>
                <div class="col-md-6 col-lg-3 text-center">
                    <div class="circleDiv text-center">
                        <img src="img/<?php echo $howItworkscol[$i][0]?>.svg" alt="">
                        <span class="numberS"><?php echo $howItworkscol[$i][1]?></span>
                    </div>
                    <article>
                        <h5 class="h5 py-2"><?php echo $howItworkscol[$i][2]?></h5>
                        <p><?php echo $howItworkscol[$i][3]?></p>
                    </article>
                </div>
               <?php } ?>
                <!--/ col -->             
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </section>
    <!--/ how it works -->
    <!--/ main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php' ?>
</body>

</html>
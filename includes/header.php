 <!-- header starts -->
    <header>
        <div class="container position-relative">
            <div class="navbar navbar-expand-lg bsnav bsnav-light">
                <a class="navbar-brand" href="#">
                    <img src="img/logo.svg" alt="">
                </a>
                <button class="navbar-toggler toggler-spring"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse justify-content-between">
                    <ul class="navbar-nav navbar-mobile ps-3">
                        <li class="nav-item"><a class="activeNav" href="index.php">Home</a></li>
                        <li class="nav-item"><a class="nav-link" href="about.php">About</a></li>
                        <li class="nav-item"><a class="nav-link" href="contact.php">Contact</a></li>
                        <li class="nav-item"><a class="nav-link" href="joinasbusiness.php">Join as Business</a></li>
                        <li class="nav-item"><a class="nav-link" href="customerRegistration.php">Register as User</a></li>
                    </ul>
                    <ul class="navbar-nav navbar-mobile">
                        <li class="nav-item">
                            <a class="nav-link loginlink" href="campaigns.php"> Campaigns</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link loginlink" href="login.php"> Login</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="bsnav-mobile">
                <div class="bsnav-mobile-overlay"></div>
                <div class="navbar"></div>
            </div>
        </div>
    </header>
    <!--/header ends -->
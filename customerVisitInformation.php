<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RS Hospitality</title>
     <?php include 'includes/styles.php' ?>
     <?php include 'includes/arrayObjects.php' ?>
</head>

<body>    
   <?php include 'includes/headerpostLogin.php'?>
    <!-- sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="subpageHeader">
            <div class="container">
                <h1 class="h1">Customer Name will be here</h1>
            </div>
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpageBody">
            <!-- container -->
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                                <!-- <li class="breadcrumb-item"><a href="#">Library</a></li> -->
                                <li class="breadcrumb-item active" aria-current="page">My Profile</li>
                            </ol>
                        </nav>
                    </div>
                </div>
                <!--/ row -->
                <div class="row">
                    <!-- col left nav bar -->
                    <div class="col-md-12">
                       <div class="leftNav">
                           <?php include 'includes/userProfileNav.php'?>
                       </div>
                    </div>
                    <!--/ col left nav bar -->

                    <!-- right content -->
                    <div class="col-md-12">
                        <div class="rightProfile">
                           <div class="titleProfile d-flex justify-content-between">
                                <h4 class="fbold sectionTitle">My Visitings</h4>
                                <a href="javascript:void(0)" class="filledLink" data-bs-toggle="modal" data-bs-target="#editProfile">Add Visit</a>
                           </div>
						   
                            <!-- right profile body -->
                            <div class="rightProfileBody">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table" id="example">
                                                <thead class="table-dark">
                                                    <tr>
                                                        <th scope="col">Visit Date</th>
                                                        <th scope="col">Store</th>
                                                        <th scope="col">Day</th>
                                                        <th scope="col">Amount</th>
                                                        <th scope="col">Receipt No:</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>15-08-2021</td>
                                                        <td>123#</td>
                                                        <td>Tuesday</td>
                                                        <td>$100</td>
                                                        <td>1253664</td>
                                                    </tr>
                                                    <tr>
                                                        <td>15-08-2021</td>
                                                        <td>123#</td>
                                                        <td>Tuesday</td>
                                                        <td>$100</td>
                                                        <td>1253664</td>
                                                    </tr>
                                                    <tr>
                                                        <td>15-08-2021</td>
                                                        <td>123#</td>
                                                        <td>Tuesday</td>
                                                        <td>$100</td>
                                                        <td>1253664</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/ right profile body -->
                        </div>
                    </div>
                    <!--/ right content -->
                </div>
                <!--/ row -->             
            </div>
            <!--/ container -->
        </div>        
        <!--/ sub page body -->
    </main>
    
    <!--/ sub page main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php' ?>

    <!-- Modal -->
    <div class="modal fade" id="editProfile" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-fullscreen">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Store Visit</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <!-- form start -->
                    <form class="form" method="post">
                        <div class="row">                        
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Store</label>
                                    <div class="input-group">
                                       <select class="form-control">
                                            <option>Select Store</option>
                                       </select>
                                    </div>
                                </div>
                            </div>
                             <div class="col-md-4">
                                <div class="form-group">
                                    <label>Visit Date</label>
                                    <div class="input-group">
                                       <input type="date" class="form-control" name="" placeholder="Select Date">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Amount</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="" placeholder="Ex: $250">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Upload Receipt</label>
                                    <div class="input-group">
                                        <input type="file" class="form-control" name="" placeholder="Upload receipt">
                                        
                                    </div>
                                    <small>Accept only pdf, jpg</small>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--/ form ends -->               
                </div>
            </div>
                <div class="modal-footer">
                     <!-- <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button> -->
                     <button type="button" class="btn btn-primary filledLink">Save</button>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
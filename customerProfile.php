<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RS Hospitality</title>
     <?php include 'includes/styles.php' ?>
     <?php include 'includes/arrayObjects.php' ?>
</head>

<body>    
   <?php include 'includes/headerpostLogin.php'?>
    <!-- sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="subpageHeader">
            <div class="container">
                <h1 class="h1">Customer Name will be here</h1>
            </div>
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpageBody">
            <!-- container -->
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                                <!-- <li class="breadcrumb-item"><a href="#">Library</a></li> -->
                                <li class="breadcrumb-item active" aria-current="page">My Profile</li>
                            </ol>
                        </nav>
                    </div>
                </div>
                <!--/ row -->
                <div class="row">
                    <!-- col left nav bar -->
                    <div class="col-md-12">
                       <div class="leftNav">
                            <?php include 'includes/userProfileNav.php'?>
                       </div>
                    </div>
                    <!--/ col left nav bar -->

                    <!-- right content -->
                    <div class="col-md-12">
                        <div class="rightProfile">
                           <div class="titleProfile d-flex justify-content-between">
                                <h4 class="fbold sectionTitle">My Profile</h4>
                                <a href="javascript:void(0)" class="filledLink" data-bs-toggle="modal" data-bs-target="#editProfile">Edit</a>
                           </div>

                            <!-- right profile body -->
                            <div class="rightProfileBody">
                                <div class="row">
                                    <div class="col-md-6 col-lg-4 proCol">
                                        <label>First Name</label>
                                        <p>Praveen</p>
                                    </div>
                                     <div class="col-md-6 col-lg-4 proCol">
                                        <label>Middle Name</label>
                                        <p>Guptha</p>
                                    </div>
                                    <div class="col-md-6 col-lg-4 proCol">
                                        <label>Last Name</label>
                                        <p>Nandipati</p>
                                    </div>
                                     <div class="col-md-6 col-lg-4 proCol">
                                        <label>Phone Number</label>
                                        <p>+91 9642123254</p>
                                    </div>
                                     <div class="col-md-6 col-lg-4 proCol">
                                        <label>Email</label>
                                        <p>praveennandipati@gmail.com</p>
                                    </div>
                                    <div class="col-md-6 col-lg-4 proCol">
                                        <label>City Name</label>
                                        <p>city Name will be here</p>
                                    </div>
                                     <div class="col-md-6 col-lg-4 proCol">
                                        <label>address</label>
                                        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Harum possimus.</p>
                                    </div>
                                     <div class="col-md-6 col-lg-4 proCol">
                                        <label>State</label>
                                        <p>State Name</p>
                                    </div>
                                     <div class="col-md-6 col-lg-4 proCol">
                                        <label>Zip Code</label>
                                        <p>Zip Code</p>
                                    </div>
                                     <div class="col-md-6 col-lg-4 proCol">
                                        <label>Favourite Food1</label>
                                        <p>Favourite Food1</p>
                                    </div>
                                    <div class="col-md-6 col-lg-4 proCol">
                                        <label>Favourite Food2</label>
                                        <p>Favourite Food2</p>
                                    </div>
                                    <div class="col-md-6 col-lg-4 proCol">
                                        <label>Favourite Food3</label>
                                        <p>Favourite Food3</p>
                                    </div>
                                </div>
                            </div>
                            <!--/ right profile body -->
                        </div>
                    </div>
                    <!--/ right content -->
                </div>
                <!--/ row -->             
            </div>
            <!--/ container -->
        </div>        
        <!--/ sub page body -->
    </main>
    
    <!--/ sub page main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php' ?>

    <!-- Modal -->
    <div class="modal fade" id="editProfile" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-fullscreen">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Profile</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <!-- form start -->
                    <form class="form" method="post">
                        <div class="row">                        
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>First Name</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="" placeholder="Write First Name">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Middle intial</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="" placeholder="Write Middle intial">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="" placeholder="Last Name">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Phone</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="" placeholder="Phone Number">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>City </label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="" placeholder="City ">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>State </label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="" placeholder="State ">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Address Line</label>
                                    <div class="input-group">
                                        <textarea class="form-control" style="height:70px;"></textarea>
                                    </div>
                                </div>
                            </div>                        
                        
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Zip Code </label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="" placeholder="Zip Code ">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Email </label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="" placeholder="Email">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Favourite Food1 </label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="" placeholder="Favourite Food1 ">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Favourite Food2 </label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="" placeholder="Favourite Food2 ">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Favourite Food3 </label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="" placeholder="Favourite Food3 ">
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </form>
                    <!--/ form ends -->               
                </div>
            </div>
                <div class="modal-footer">
                     <!-- <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button> -->
                     <button type="button" class="btn btn-primary filledLink">Save</button>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
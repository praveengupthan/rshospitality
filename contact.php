<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RS Hospitality</title>
     <?php include 'includes/styles.php' ?>
     <?php include 'includes/arrayObjects.php' ?>
</head>

<body>    
   <?php include 'includes/header.php'?>
    <!-- sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="subpageHeader">
            <div class="container">
                <h1 class="h1">Contact</h1>
            </div>
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpageBody">
            <div class="container">

            <!-- row -->
            <div class="row">
                <div class="col-md-4">
                    <div class="contactCol">
                        <div class="icon">
                            <span class="icon-map-marker icomoon"></span>
                        </div>
                        <p class="text-center px-5">PO Box 16122 Collins Street West Victoria 8007 Australia</p>
                    </div>
                </div>
                 <div class="col-md-4">
                    <div class="contactCol">
                        <div class="icon">
                            <span class="icon-phone icomoon"></span>
                        </div>
                        <p class="text-center pb-0 mb-0">+01 123 456 789</p>
                        <p class="text-center">+01 987 654 321</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="contactCol">
                        <div class="icon">
                            <span class="icon-paper-plane icomoon"></span>
                        </div>
                        <p class="text-center pb-0 mb-0">mail@domnoo.com</p>
                        <p class="text-center">mail@domnoo.com</p>
                    </div>
                </div>
            </div>
            <!--/ row -->

            <!-- row -->
            <div class="row justify-content-center py-5">
                <div class="col-md-6 text-center">
                    <h3 class="text-center">Send us a messages</h3>
                    <p class="text-center">Sed scelerisque, ipsum in rutrum gravida, odio eros maximus erat, varius pretium tellus
eros et quam. </p>
                </div>
            </div>
            <!--/ row -->

            <div class="row justify-content-center">
                <div class="col-md-10">
                     <!-- row -->
                    <div class="row justify-content-center">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>First Name</label>
                                <div class="input-group">
                                    <input type="text" placeholder="Write Your Name" name="" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Last Name</label>
                                <div class="input-group">
                                    <input type="text" placeholder="Write Your Last Name" name="" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Email</label>
                                <div class="input-group">
                                    <input type="text" placeholder="Email" name="" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Phone Number</label>
                                <div class="input-group">
                                    <input type="text" placeholder="Phone Number" name="" class="form-control">
                                </div>
                            </div>
                        </div>

                         <div class="col-md-12">
                            <div class="form-group">
                                <label>Message</label>
                                <div class="input-group">
                                   <textarea class="form-control" style="height:150px;"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <button class="borderLink">Submit</button>
                        </div>


                    </div>
                    <!--/ row -->
                </div>
            </div>
            </div>
        </div>
        <!--/ sub page body -->
    </main>
    
    <!--/ sub page main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php' ?>
</body>

</html>
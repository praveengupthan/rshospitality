<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RS Hospitality</title>
     <?php include 'includes/styles.php' ?>
     <?php include 'includes/arrayObjects.php' ?>
</head>

<body>    
   <?php include 'includes/header.php'?>
    <!-- sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="subpageHeader">
            <div class="container">
                <h1 class="h1">About</h1>
            </div>
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpageBody">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <img src="img/img12.jpg" alt="" class="img-fluid">
                    </div>
                    <div class="col-md-6 align-self-center">
                        <h5 class="text-uppercase fred h6">Lorem, ipsum dolor Culpa, obcaecati.</h5>
                        <h2 class="h2 pb-3">Lorem ipsum dolor sit amet consectetur.</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam eaque voluptatum corporis iure quaerat quod vero corrupti saepe natus, illum, modi ad, nostrum asperiores obcaecati sapiente perferendis praesentium ut molestiae quia! Rem, ab nobis aliquam nemo laborum nisi dolore voluptates? Fugiat necessitatibus debitis quam et obcaecati eos laboriosam excepturi soluta ipsum illo quos odit veritatis velit quo nemo minus sapiente accusamus ducimus rem a qui, id tempore. veritatis velit quo nemo minus sapiente accusamus ducimus rem a qui, id tempore. </p>
                        
                        <ul>
                            <li>Lorem ipsum dolor sit amet.</li>
                            <li>Lorem ipsum dolor sit amet consectetur.</li>
                            <li>Lorem ipsum dolor sit.</li>
                            <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eaque, asperiores!</li>
                        </ul>
                    </div>
                </div>

                 <div class="row py-5">
                    <div class="col-md-6 order-md-last">
                        <img src="img/about02.jpg" alt="" class="img-fluid">
                    </div>
                    <div class="col-md-6 align-self-center">
                        <h5 class="text-uppercase fred h6">Lorem, ipsum dolor Culpa, obcaecati.</h5>
                        <h2 class="h2 pb-3">Lorem ipsum dolor sit amet consectetur.</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam eaque voluptatum corporis iure quaerat quod vero corrupti saepe natus, illum, modi ad, nostrum asperiores obcaecati sapiente perferendis praesentium ut molestiae quia! Rem, ab nobis aliquam nemo laborum nisi dolore voluptates? Fugiat necessitatibus debitis quam et obcaecati eos laboriosam excepturi soluta ipsum illo quos odit veritatis velit quo nemo minus sapiente accusamus ducimus rem a qui, id tempore. veritatis velit quo nemo minus sapiente accusamus ducimus rem a qui, id tempore. </p>
                    </div>
                </div>


            </div>
        </div>
        <!--/ sub page body -->
    </main>
    
    <!--/ sub page main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php' ?>
</body>

</html>
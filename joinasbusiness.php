<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RS Hospitality</title>
     <?php include 'includes/styles.php' ?>
     <?php include 'includes/arrayObjects.php' ?>
</head>

<body>    
   <?php include 'includes/header.php'?>
    <!-- sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="subpageHeader">
            <div class="container">
                <h1 class="h1">Join as Business</h1>
            </div>
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpageBody">
            <div class="container">

                <h2>Register your Business and Join with us</h2>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sed, neque!</p>

                <!-- form start -->
                <form class="form" method="post">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Company Name</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="" placeholder="Write Your Business Name">
                                </div>
                            </div>
                        </div>
                         <div class="col-md-4">
                            <div class="form-group">
                                <label>First Name</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="" placeholder="Write First Name">
                                </div>
                            </div>
                        </div>
                         <div class="col-md-4">
                            <div class="form-group">
                                <label>Middle intial</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="" placeholder="Write Middle intial">
                                </div>
                            </div>
                        </div>
                         <div class="col-md-4">
                            <div class="form-group">
                                <label>Last Name</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="" placeholder="Last Name">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Phone</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="" placeholder="Phone Number">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>City </label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="" placeholder="City ">
                                </div>
                            </div>
                        </div>
                         <div class="col-md-12">
                            <div class="form-group">
                                <label>Address Line</label>
                                <div class="input-group">
                                     <textarea class="form-control" style="height:70px;"></textarea>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>State </label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="" placeholder="State ">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Zip Code </label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="" placeholder="Zip Code ">
                                </div>
                            </div>
                        </div>
                         <div class="col-md-4">
                            <div class="form-group">
                                <label>Email </label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="" placeholder="Email">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Password </label>
                                <div class="input-group">
                                    <input type="password" class="form-control" name="" placeholder="Create Password">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Confirm Password </label>
                                <div class="input-group">
                                    <input type="password" class="form-control" name="" placeholder="Confirm Password">
                                </div>
                            </div>
                        </div>
                         <div class="col-md-12 pt-3">
                            <button class="filledLink">Create Business Profile</button>
                        </div>
                        
                    </div>
                </form>
                <!--/ form ends -->
               
            </div>
        </div>        
        <!--/ sub page body -->
    </main>
    
    <!--/ sub page main -->
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php' ?>
</body>

</html>